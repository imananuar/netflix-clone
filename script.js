let nav = document.querySelector('nav');
window.onscroll = function() {

    if (document.documentElement.scrollTop > 50) {
        if (nav.classList.contains('nav-transparent')) {
            nav.classList.remove("nav-transparent");
        }
        nav.classList.add('nav-colored');
    } else if (document.documentElement.scrollTop < 50 && nav.classList.contains('nav-colored')) {
        nav.classList.remove('nav-colored');
        nav.classList.add('nav-transparent')
    }
}

$('.owl-carousel').owlCarousel({
    loop:true,
    stagePadding: 50,
    margin:10,
    nav:true,
    slideBy: 6,
    responsive:{
        0:{
            items:2
        },
        500:{
            items:3,
        },
        1100:{
            items:6
        }
    }
});